# kicad-raptor-nano

## Pinout for Nucleo LxxxKx board

| raptor   | nano      | Nucleo-LxxxKx | alternate functions                                                                                             | additiona functions                                       |
|----------|-----------|---------------|-----------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| TX       | 1 (D1)    | PA9           | TIM1_CH2, I2C1_SCL, **USART1_TX**, SAI1_FS_A, TIM15_BKIN, EVENTOUT                                                  |                                                           |
| RX       | 2 (D0)    | PA10          | TIM1_CH3, I2C1_SDA, **USART1_RX**, USB_CRS_SYNC, SAI1_SD_A, EVENTOUT                                                |                                                           |
| RST      | 3 (RST)   | NRST          |                                                                                                                 |                                                           |
| GND      | 4 (GND)   | GND           |                                                                                                                 |                                                           |
|          | 5 (D2)    | PA12          | TIM1_ETR, SPI1_MOSI, USART1_RTS_DE, CAN1_TX, USB_DP, EVENTOUT                                                   |                                                           |
| DRV1_BI1 | 6 (D3)~   | PB0           | **TIM1_CH2N**, SPI1_NSS, USART3_CK, QUADSPI_BK1_IO1, COMP1_OUT, SAI1_EXTCLK, EVENTOUT                               | ADC1_IN15                                                 |
| ENC_1A   | 7 (D4)    | PB7           | LPTIM1_IN2, I2C1_SDA, USART1_RX, TSC_G2_IO4, EVENTOUT                                                           | COMP2_INM, PVD_IN                                         |
| TURBINE  | 8 (D5)~   | PB6           | LPTIM1_ETR, I2C1_SCL, USART1_TX, TSC_G2_IO3, SAI1_FS_B, **TIM16_CH1N**, EVENTOUT                                    | COMP2_INP                                                 |
| DRV1_FI1 | 9 (D6)~   | PB1           | **TIM1_CH3N**, USART3_RTS_DE, LPUART1_RTS_DE, QUADSPI_BK1_IO0, LPTIM2_IN1, EVENTOUT                                 | COMP1_INM, ADC1_IN16                                      |
| ENC_1B   | 10 (D7)   | PC14          | EVENTOUT                                                                                                        | OSC32_IN                                                  |
| ENC_2A   | 11 (D8)   | PC15          | EVENTOUT                                                                                                        | OSC32_OUT                                                 |
| DRV2_BI1 | 12 (D9)~  | PA8           | MCO, **TIM1_CH1**, USART1_CK, SWPMI1_IO, SAI1_SCK_A, LPTIM2_OUT, EVENTOUT                                           |                                                           |
| DRV2_FI1 | 13 (D10)~ | PA11          | **TIM1_CH4**, TIM1_BKIN2, SPI1_MISO, COMP1_OUT, USART1_CTS, CAN1_RX, USB_DM, TIM1_BKIN2_COMP1, EVENTOUT             |                                                           |
| ENC_2B   | 14 (D11)~ | PB5           | LPTIM1_IN1, I2C1_SMBA, SPI1_MOSI, SPI3_MOSI, USART1_CK, TSC_G2_IO2, COMP2_OUT, SAI1_SD_B, TIM16_BKIN, EVENTOUT  |                                                           |
| BTN_1    | 15 (D12)  | PB4           | NJTRST, I2C3_SDA, SPI1_MISO, SPI3_MISO, USART1_CTS, TSC_G2_IO1, SAI1_MCLK_B, EVENTOUT                           | COMP2_INP                                                 |
| (LED)    | 16 (D13)  | PB3           | JTDO-TRACESWO, TIM2_CH2, SPI1_SCK, SPI3_SCK, USART1_RTS_DE, SAI1_SCK_B, EVENTOUT                                | COMP2_INM                                                 |
| 3V3      | 17 (3V3)  | 3V3           |                                                                                                                 |                                                           |
| AVDD     | 18 (AREF) | VDDA          |                                                                                                                 |                                                           |
| IR1      | 19 (A0)   | PA0           | TIM2_CH1, USART2_CTS, COMP1_OUT, SAI1_EXTCLK, TIM2_ETR, EVENTOUT                                                | OPAMP1_VINP, COMP1_INM, **ADC1_IN5**, RTC_TAMP2, WKUP1, CK_IN |
| IR2      | 20 (A1)   | PA1           | TIM2_CH2, I2C1_SMBA, SPI1_SCK, USART2_RTS_DE, TIM15_CH1N, EVENTOUT                                              | OPAMP1_VINM, COMP1_INP, **ADC1_IN6**                          |
| IR3      | 21 (A2)   | PA3           | TIM2_CH3, USART2_TX, LPUART1_TX, QUADSPI_BK1_NCS, COMP2_OUT, TIM15_CH1, EVENTOUT                                | OPAMP1_VOUT, COMP2_INP, **ADC1_IN8**                          |
| IR4      | 22 (A3)   | PA4           | SPI1_NSS, SPI3_NSS, USART2_CK, SAI1_FS_B, LPTIM2_OUT, EVENTOUT                                                  | COMP1_INM, COMP2_INM, **ADC1_IN9**, DAC1_OUT1                 |
| IR5      | 23 (A4)   | PA5           | TIM2_CH1, TIM2_ETR, SPI1_SCK, LPTIM2_ETR, EVENTOUT                                                              | COMP1_INM, COMP2_INM, **ADC1_IN10**, DAC1_OUT2                |
| IR6      | 24 (A5)   | PA6           | TIM1_BKIN, SPI1_MISO, COMP1_OUT, USART3_CTS, LPUART1_CTS, QUADSPI_BK1_IO3, TIM1_BKIN_COMP2, TIM16_CH1, EVENTOUT | **ADC1_IN11**                                                 |
| IR7      | 25 (A6)   | PA7           | TIM1_CH1N, I2C3_SCL, SPI1_MOSI, QUADSPI_BK1_IO2, COMP2_OUT, EVENTOUT                                            | **ADC1_IN12**                                                 |
| IR8      | 26 (A7)   | PA2           | TIM2_CH3, USART2_TX, LPUART1_TX, QUADSPI_BK1_NCS, COMP2_OUT, TIM15_CH1, EVENTOUT                                | COMP2_INM, **ADC1_IN7**, WKUP4, LSCO                          |
| 5V       | 27 (5V)   | 5V            |                                                                                                                 |                                                           |
| RST      | 28 (RST)  | NRST          |                                                                                                                 |                                                           |
| GND      | 29 (GND)  | GND           |                                                                                                                 |                                                           |
| VIN      | 30 (VIN)  | VIN           |                                                                                                                 |                                                           |
